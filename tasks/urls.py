from django.urls import path

from .views import CreateTaskView, MyTasksListView

app_name = "tasks"

urlpatterns = [
    path("create/", CreateTaskView.as_view(), name="create_task"),
    path("mine/", MyTasksListView.as_view(), name="show_my_tasks"),
]
