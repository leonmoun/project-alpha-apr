from .models import Task

from django.contrib import admin


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "start_date",
        "due_date",
        "is_completed",
        "project",
        "assignee",
    )
    list_filter = ("is_completed", "project")
    search_fields = ("name", "project__name", "assignee__username")
