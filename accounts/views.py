from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import logout, authenticate, login as auth_login
from django.shortcuts import render, redirect
from django.views.decorators.http import require_http_methods
from django.urls import reverse
from .forms import LoginForm, SignupForm
from projects.models import Project


@require_http_methods(["GET", "POST"])
def login_view(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                auth_login(request, user)
                return redirect(reverse("projects:list_projects"))
            else:
                form.add_error(None, "Invalid username or password.")
    else:
        form = LoginForm()

    return render(request, "accounts/login.html", {"form": form})


@login_required
def list_projects(request):
    projects = Project.objects.filter(members=request.user)
    return render(
        request, "projects/list_projects.html", {"projects": projects}
    )


@login_required
def logout_view(request):
    logout(request)
    return redirect("login")


@require_http_methods(["GET", "POST"])
def signup_view(request):
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password != password_confirmation:
                form.add_error(
                    "password_confirmation", "The passwords do not match"
                )
                return render(
                    request, "registration/signup.html", {"form": form}
                )

            user = User.objects.create_user(
                username=username, password=password
            )
            auth_login(request, user)
            return redirect(reverse("list_projects"))
    else:
        form = SignupForm()

    return render(request, "registration/signup.html", {"form": form})
