from django.contrib import admin
from django.urls import path, include
from django.views.generic import RedirectView

from projects.views import (
    ProjectListView,
    ProjectDetailView,
    ProjectCreateView,
)

urlpatterns = [
    path("admin/", admin.site.urls),
    path("accounts/", include("accounts.urls")),
    path("projects/", include("projects.urls", namespace="projects")),
    path("tasks/", include("tasks.urls", namespace="tasks")),
    path("list-projects/", ProjectListView.as_view(), name="list_projects"),
    path(
        "projects/<int:id>/", ProjectDetailView.as_view(), name="show_project"
    ),
    path(
        "projects/create/", ProjectCreateView.as_view(), name="create_project"
    ),
    path(
        "",
        RedirectView.as_view(
            pattern_name="projects:list_projects", permanent=False
        ),
        name="home",
    ),
]
