from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import ListView, CreateView
from .forms import ProjectForm
from .models import Project


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/project_list.html"
    context_object_name = "projects"

    def get_queryset(self):
        user = self.request.user
        return Project.objects.filter(owner=user)


class ProjectDetailView(LoginRequiredMixin, View):
    def get(self, request, id):
        project = get_object_or_404(Project, id=id)
        return render(
            request, "projects/project_detail.html", {"project": project}
        )


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    form_class = ProjectForm
    template_name = "projects/create_project.html"
    success_url = reverse_lazy("list_projects")

    def form_valid(self, form):
        form.instance.owner = self.request.user
        super().form_valid(form)
        return redirect(self.get_success_url())
